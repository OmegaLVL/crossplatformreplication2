#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "GameObject.h"
#include "Player.h"
#include "World.h"
#include "Colors.h"
#include "Maths.h"
#include "Ball.h"

#include <iostream>
#include <fstream>
#include <thread>

#include "CapacityTestHarness.h"

//Constructor
CapacityTestHarness::CapacityTestHarness()
{

}

//Deconstructor
CapacityTestHarness::~CapacityTestHarness()
{

}

void CapacityTestHarness::SetUp()
{
    GameObjectRegistry::StaticInit();
    World::StaticInit();
    GameObjectRegistry::sInstance->RegisterCreationFunction('PLYR', &Player::StaticCreatePtr);
    GameObjectRegistry::sInstance->RegisterCreationFunction('BALL', &Player::StaticCreatePtr);
}


void CapacityTestHarness::TearDown()
{

}

