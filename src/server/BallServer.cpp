#include "BallServer.h"
#include "PlayerServer.h"
#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"

BallServer::BallServer()
{
	mTimeToDie = Timing::sInstance.GetFrameStartTime() + 1.f;
}

void BallServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}


void BallServer::Update()
{
	Ball::Update();

	if (Timing::sInstance.GetFrameStartTime() > mTimeToDie)
	{
		SetDoesWantToDie(true);
	}

}

bool BallServer::HandleCollisionWithPlayer(Player* inPlayer)
{
	if (inPlayer->GetPlayerId() != GetPlayerId())
	{
		//kill yourself!
		SetDoesWantToDie(true);

		static_cast<PlayerServer*>(inPlayer)->TakeDamage(GetPlayerId());

	}

	return false;
}
