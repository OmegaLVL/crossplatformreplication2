#ifndef LOCKSTEP_TEST_HARNESS_H_
#define LOCKSTEP_TEST_HARNESS_H_
#pragma once

#include <limits.h>
#include <gtest/gtest.h>

class LockstepTestHarness : public ::testing::Test
{
protected:

	virtual void SetUp();
	virtual void TearDown();

	// No test object here - its a Singleton.

	GameObjectPtr go;

public:
	LockstepTestHarness();
	virtual ~LockstepTestHarness();
};

#endif // LOCKSTEP_TEST_HARNESS_H_

