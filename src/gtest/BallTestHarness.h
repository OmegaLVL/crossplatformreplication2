#ifndef BALL_TESTHARNESS_H_
#define BALL_TESTHARNESS_H_

#include <limits.h>
#include <gtest/gtest.h>

#include "Ball.h"

class BallTestHarness : public ::testing::Test
{
protected:

  virtual void SetUp();
  virtual void TearDown();

  BallPtr bp;

public:

    BallTestHarness();
    virtual ~BallTestHarness();
};

#endif // BALL_TESTHARNESS_H_
