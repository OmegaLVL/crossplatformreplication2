// #include <limits.h>
// #include <math.h>
// #include "gtest/gtest.h"

// #include "ProjectTestHarness.h"
// #include "GameObjectTestHarness.h"
// #include "Move.h"
// #include "TestObject.h"
// #include "GameObject.h"
// #include "Colors.h"
// #include "Maths.h"
// #include "PlayerTestHarness.h"
// #include "BallTestHarness.h"


// #include <iostream>
// #include <fstream>
// #include <thread>

// /* Reference: http://www.yolinux.com/TUTORIALS/Cpp-GoogleTest.html */

// ProjectTestHarness::ProjectTestHarness()
// {}

// ProjectTestHarness::~ProjectTestHarness()
// {}


// //Standard set up and tear down to destroy objects.  Nothing created by ProjectTestHarness, as each harness has their own set up and tear down.
// void ProjectTestHarness::SetUp()
// {}

// void ProjectTestHarness::TearDown()
// {}

// //TEST 1 - MOVEMENT - How can we get the server to drop packets from movement?
// //Testing the Constructors - We could try to slow this down or speed it up.
// TEST_F(ProjectTestHarness,TestDefaultConstructor)
// {

//   EXPECT_FLOAT_EQ(m->GetTimestamp(), 0.0f);
//   EXPECT_FLOAT_EQ(m->GetDeltaTime(), 0.0f);
// }

// //We'll see what happens when timestamp and deltatime as fairly far removed from each other.
// TEST_F(ProjectTestHarness,TestOverloadedConstructor)
// {

//   EXPECT_FLOAT_EQ(m2->GetTimestamp(), 2.0f);
//   EXPECT_FLOAT_EQ(m2->GetDeltaTime(), 0.5f);
// }


// //Testing the Serialisation and Deserialisation of movement - We can change the buffer size to see if packets are dropped.
// TEST_F(ProjectTestHarness,TestSerialiseAndDeserialiseMove)
// {
//   std::shared_ptr<OutputMemoryBitStream> out;
//   std::shared_ptr<InputMemoryBitStream> in;

//   //Changed buffer size to '256'
//   const int BUFF_MAX = 256;
//   char* bigBuffer = new char[BUFF_MAX]; //testing only - gets replaced.
//   in.reset(new InputMemoryBitStream(bigBuffer,BUFF_MAX));
//   out.reset(new OutputMemoryBitStream());

//   MovePtr writeOut;
//   writeOut.reset(new Move());

//   EXPECT_EQ(m->GetTimestamp(), writeOut->GetTimestamp());

//   //restrict to time detal
//   writeOut->SetTimestamp(3.3f);

//   EXPECT_NE(m->GetTimestamp(), writeOut->GetTimestamp()); //with one changed should be different.

//   //write it into a buffer.
//   writeOut->Write(*out);

//   // ... imagine networking goes on and we get an
//   // actually we're connecting the output buffer to the input.
//   // copy the buffer first (or we get double de-allocation)
//   int copyLen = out->GetByteLength();
//   char* copyBuff = new char[copyLen];
//   memcpy(copyBuff, out->GetBufferPtr(), copyLen);

//   in.reset(new InputMemoryBitStream(copyBuff, copyLen));

//   // update from our server.
//   m->Read(*in);

//   // expect these to now be the same.
//   EXPECT_EQ(m->GetTimestamp(), writeOut->GetTimestamp()); //expect constructed objs to be the same.
// }


// //TEST 2 - GAME OBJECTS - Let's test our objects!
// TEST_F(GameObjectTestHarness, constructor_noArgs)
// {
//     // Check defaults are set
//     EXPECT_TRUE(Maths::Is3DVectorEqual(go->GetColor(), Colors::White));
//     EXPECT_TRUE(Maths::Is3DVectorEqual(go->GetLocation(), Vector3::Zero));
//     EXPECT_FLOAT_EQ(go->GetCollisionRadius(), 0.5f);
//     EXPECT_FLOAT_EQ(go->GetScale(), 1.0f);
//     EXPECT_FLOAT_EQ(go->GetRotation(), 0.0f);
//     EXPECT_EQ(go->GetIndexInWorld(), -1);
//     EXPECT_EQ(go->GetNetworkId(), 0);

//     //Check our macro has worked.
//     EXPECT_EQ(go->GetClassId(), 'GOBJ');
//     EXPECT_NE(go->GetClassId(), 'HELP');
// }

// TEST_F(GameObjectTestHarness, GetAllStateMask)
// {
//     EXPECT_EQ(go->GetAllStateMask(), 0);
// }

// TEST_F(GameObjectTestHarness, IndexInWorld)
// {

//     //Halved test value
//     int testValue = 10;

//     //Check default before we set.
//     EXPECT_EQ(go->GetIndexInWorld(), -1);

//     // Set the value.
//     go->SetIndexInWorld(testValue);

//     //Check its been set.
//     EXPECT_EQ(go->GetIndexInWorld(), testValue);
// }

// TEST_F(GameObjectTestHarness, Rotation)
// {
//     float testValue = 2.0 * Maths::PI;

//     //Check default before we set.
//     EXPECT_FLOAT_EQ(go->GetRotation(), 0.0f);

//     // Set the value.
//     go->SetRotation(testValue);

//     //Check its been set.
//     EXPECT_FLOAT_EQ(go->GetRotation(), testValue);
// }

// TEST_F(GameObjectTestHarness, Scale)
// {
//     float testValue = 2.0f;

//     //Check default before we set.
//     EXPECT_FLOAT_EQ(go->GetScale(), 1.0f);

//     // Set the value.
//     go->SetScale(testValue);

//     //Check its been set.
//     EXPECT_FLOAT_EQ(go->GetScale(), testValue);
// }

// TEST_F(GameObjectTestHarness, Location)
// {
//     Vector3 testValue(1.1f, 2.2f, 3.3f);

//     //Check default before we set.
//     EXPECT_TRUE(Maths::Is3DVectorEqual(go->GetLocation(), Vector3::Zero));

//     // Set the value.
//     go->SetLocation(testValue);

//     //Check its been set.
//     EXPECT_TRUE(Maths::Is3DVectorEqual(go->GetLocation(), testValue));
// }

// TEST_F(GameObjectTestHarness, CollisionRadius)
// {
//     float testValue = 2.0f;

//     //Check default before we set.
//     EXPECT_FLOAT_EQ(go->GetCollisionRadius(), 0.5f);

//     // Set the value.
//     go->SetCollisionRadius(testValue);

//     //Check its been set.
//     EXPECT_FLOAT_EQ(go->GetCollisionRadius(), testValue);
// }

// TEST_F(GameObjectTestHarness, ForwardVector)
// {
//     // Game is 2D
//     float rot = Maths::PI;
//     Vector3 answer(sinf(rot), -cosf(rot), 0.f);

//     go->SetRotation(Maths::PI);

//     EXPECT_TRUE(Maths::Is3DVectorEqual(go->GetForwardVector(), answer));
// }

// TEST_F(GameObjectTestHarness, Color)
// {
//     //Check default before we set.
//     EXPECT_TRUE(Maths::Is3DVectorEqual(go->GetColor(), Colors::White));

//     // Set the value.
//     go->SetColor(Colors::Black);

//     //Check its been set.
//     EXPECT_TRUE(Maths::Is3DVectorEqual(go->GetColor(), Colors::Black));
// }

// TEST_F(GameObjectTestHarness, WantsToDie)
// {
//     bool testValue = true;

//     //Check default before we set.
//     EXPECT_FALSE(go->DoesWantToDie());

//     // Set the value.
//     go->SetDoesWantToDie(testValue);

//     //Check its been set.
//     EXPECT_EQ(go->DoesWantToDie(), testValue);
// }

// TEST_F(GameObjectTestHarness, NetworkId)
// {
//     int testValue = 20;

//     //Check default before we set.
//     EXPECT_EQ(go->GetNetworkId(), 0);

//     // Set the value.
//     go->SetNetworkId(testValue);

//     //Check its been set.
//     EXPECT_EQ(go->GetNetworkId(), testValue);
// }

// //TEST_F(GameObjectTestHarness, EqualsOperator1)
// //{ /* Won't compile - why?
// //  GameObject a ();
// //  GameObject b ();
// //
// //  a.SetNetworkId(10);
// //  b.SetNetworkId(10);
// //
// //  EXPECT_TRUE(a == b); */
// //}

// TEST_F(GameObjectTestHarness, EqualsOperator2)
// {
//     GameObject* a = new GameObject();
//     GameObject* b = new GameObject();

//     a->SetNetworkId(10);
//     b->SetNetworkId(10);

//     EXPECT_TRUE(*a == *b);
// }

// TEST_F(GameObjectTestHarness, EqualsOperator3)
// {
//     GameObject* a = new GameObject();
//     GameObject* b = new GameObject();

//     a->SetNetworkId(10);
//     b->SetNetworkId(30);

//     EXPECT_FALSE(*a == *b);
// }


// TEST_F(GameObjectTestHarness, EqualsOperator4)
// {
//     GameObjectPtr b(new GameObject());

//     go->SetNetworkId(10);
//     b->SetNetworkId(10);

//     EXPECT_TRUE(*go == *b);
// }


// //TEST 3 - PLAYER
// TEST_F(PlayerTestHarness, constructor_noArgs)
// {
//     // Check defaults are set
//     // Should be no need to do these as they were tested with the base class.
//     EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetColor(), Colors::White));
//     EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetLocation(), Vector3::Zero));
//     EXPECT_FLOAT_EQ(pp->GetCollisionRadius(), 0.5f);
//     EXPECT_FLOAT_EQ(pp->GetScale(), 1.0f);
//     EXPECT_FLOAT_EQ(pp->GetRotation(), 0.0f);
//     EXPECT_EQ(pp->GetIndexInWorld(), -1);
//     EXPECT_EQ(pp->GetNetworkId(), 0);

//     EXPECT_TRUE(Maths::Is3DVectorEqual(pp->GetVelocity(), Vector3::Zero));
//     EXPECT_EQ(pp->GetPlayerId(), 0.0f);

//     //Initial state is update all
//     int check = 0x000F; //Hex - binary 00000000 00000000 00000000 00001111
//     EXPECT_EQ(pp->GetAllStateMask(), check);

//     //Check our macro has worked.
//     EXPECT_EQ(pp->GetClassId(), 'PLYR');
//     EXPECT_NE(pp->GetClassId(), 'HELP');

//     //Added some getters so I could check these - not an easy class to test.
//     EXPECT_FLOAT_EQ(pp->GetMaxLinearSpeed(), 50.0f);
//     EXPECT_FLOAT_EQ(pp->GetMaxRotationSpeed(), 5.0f);
//     EXPECT_FLOAT_EQ(pp->GetWallRestitution(), 0.1f);
//     EXPECT_FLOAT_EQ(pp->GetNPCRestitution(), 0.1f);
//     EXPECT_FLOAT_EQ(pp->GetLastMoveTimestamp(), 0.0f);
//     EXPECT_FLOAT_EQ(pp->GetThrustDir(), 0.0f);
//     EXPECT_EQ(pp->GetHealth(), 10);
//     EXPECT_FALSE(pp->IsShooting());
// }


// /* Tests Omitted
// * There's a good chunk of this which cannot be tested in this limited example,
// * however there should be enough to underake some testing of the serialisation code.
// */

// //TEST_F(PlayerTestHarness, EqualsOperator1)
// //{ /* Won't compile - why?
// //  Player a ();
// //  Player b ();
// //
// //  a.SetPlayerId(10);
// //  b.SetPlayerId(10);
// //
// //  EXPECT_TRUE(a == b);*/
// //}

// TEST_F(PlayerTestHarness, EqualsOperator2)
// {
//     Player* a = static_cast<Player*>(Player::StaticCreate());
//     Player* b = static_cast<Player*>(Player::StaticCreate());

//     a->SetPlayerId(10);
//     b->SetPlayerId(10);

//     EXPECT_TRUE(*a == *b);
// }

// /* Need more tests here */

// TEST_F(PlayerTestHarness, EqualsOperator3)
// {
//     Player* a = static_cast<Player*>(Player::StaticCreate());
//     Player* b = static_cast<Player*>(Player::StaticCreate());

//     a->SetPlayerId(10);
//     b->SetPlayerId(30);

//     EXPECT_FALSE(*a == *b);
// }

// TEST_F(PlayerTestHarness, EqualsOperator4)
// {
//     PlayerPtr b(static_cast<Player*>(Player::StaticCreate()));

//     pp->SetPlayerId(10);
//     b->SetPlayerId(10);

//     EXPECT_TRUE(*pp == *b);
// }

// /* Serialistion tests in MemoryBitStreamTestHarness */



// //TEST 4 - Testing Ball
// TEST_F(BallTestHarness, constructor_noArgs)
// {
//     // Check defaults are set
//     EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetColor(), Colors::White));
//     EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetLocation(), Vector3::Zero));
//     EXPECT_FLOAT_EQ(bp->GetCollisionRadius(), 0.5f);
//     EXPECT_FLOAT_EQ(bp->GetScale(), 1.0f);
//     EXPECT_EQ(bp->GetIndexInWorld(), -1);
//     EXPECT_EQ(bp->GetNetworkId(), 0);

//     EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetVelocity(), Vector3::Zero));

//     //Initial state is update all
//     int check = 0x000F; //Hex - binary 00000000 00000000 00000000 00001111
//     EXPECT_EQ(bp->GetAllStateMask(), check);

//     //Check our macro has worked.
//     EXPECT_EQ(bp->GetClassId(), 'BALL');
//     EXPECT_NE(bp->GetClassId(), 'HELP');

// }
