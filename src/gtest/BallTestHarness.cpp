#include <limits.h>
#include <math.h>
#include "gtest/gtest.h"

#include "BallTestHarness.h"
#include "Ball.h"
#include "BallClient.h"
#include "TextureManager.h"
#include "Maths.h"
#include "Colors.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <iostream>
#include <fstream>
#include <thread>


BallTestHarness::BallTestHarness()
{
  bp = nullptr;
}

BallTestHarness::~BallTestHarness()
{
  bp.reset();
}

void BallTestHarness::SetUp()
{
    GameObject*	go2 = Ball::StaticCreate();
    Ball* b = static_cast<Ball*>(go2);
    this->bp.reset(b);
}

void BallTestHarness::TearDown()
{
    this->bp.reset();
    this->bp = nullptr;
}

TEST_F(BallTestHarness,constructor_noArgs)
{
  // Check defaults are set
  EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetColor(), Colors::White));
  EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetLocation(), Vector3::Zero));
  EXPECT_FLOAT_EQ(bp->GetCollisionRadius(), 0.5f);
  EXPECT_FLOAT_EQ(bp->GetScale(),1.0f);
  EXPECT_EQ(bp->GetIndexInWorld(), -1);
  EXPECT_EQ(bp->GetNetworkId(), 0);

  EXPECT_TRUE(Maths::Is3DVectorEqual(bp->GetVelocity(), Vector3::Zero));

  //Initial state is update all
  int check = 0x000F; //Hex - binary 00000000 00000000 00000000 00001111
  EXPECT_EQ(bp->GetAllStateMask(), check);

  //Check our macro has worked.
  EXPECT_EQ(bp->GetClassId(), 'BALL');
  EXPECT_NE(bp->GetClassId(), 'HELP');
  EXPECT_NE(bp->GetClassId(), 'PLYR');

}


/* Serialistion tests in MemoryBitStreamTestHarness */