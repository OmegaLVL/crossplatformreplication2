#ifndef BALL_H_
#define BALL_H_

#include "GameObject.h"
#include "World.h"
#include "Vector3.h"
#include "Player.h"

class InputState;


class Ball : public GameObject
{
public:
	CLASS_IDENTIFICATION( 'BALL', GameObject )

	//Setting some attributes and bitshifting.
	enum EBallReplicationState
	{
		EYRS_Pose = 1 << 0,
		EYRS_Color = 1 << 1,
		EYRS_PlayerId = 1 << 2,

		EYRS_AllState = EYRS_Pose | EYRS_Color | EYRS_PlayerId
	};
	

	//Create a Ball.
	static	GameObject*	StaticCreate()			{ return new Ball(); }


	//Create GO Pointer and return new Ball
	//static	GameObjectPtr	StaticCreatePtr()			{ return GameObjectPtr(new Ball()); }

	virtual uint32_t GetAllStateMask() const override { return EYRS_AllState; }
	
	virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

	//Gets and sets for ball.
	void SetVelocity(const Vector3& inVelocity) { mVelocity = inVelocity; }
	const Vector3& GetVelocity() const { return mVelocity; }

	void		SetPlayerId( uint32_t inPlayerId )			{ mPlayerId = inPlayerId; }
	uint32_t	GetPlayerId()						const 	{ return mPlayerId; }


	//Instantiating Ball from Player
	void InitFromShooter(Player* inShooter);


	//Update function.
	virtual void Update() override;

	//Handle collisions.
	virtual bool HandleCollisionWithPlayer(Player* inPlayer) override;

	
//	virtual void	Read( InputMemoryBitStream& inInputStream ) override;

protected:
	Ball();
	
	Vector3 mVelocity;
	float mMuzzleSpeed;
	uint32_t		mPlayerId;
};

typedef shared_ptr< Ball >	BallPtr;

#endif // BALL_H_
