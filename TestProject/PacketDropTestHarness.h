#ifndef PACKET_DROP_TEST_HARNESS_H_
#define PACKET_DROP_TEST_HARNESS_H_
#pragma once

#include <limits.h>
#include <gtest/gtest.h>

class PacketDropTestHarness : public ::testing::Test
{
protected:

	virtual void SetUp();
	virtual void TearDown();

	// No test object here - its a Singleton.

	GameObjectPtr go;

public:
	PacketDropTestHarness();
	virtual ~PacketDropTestHarness();
};

#endif // PACKET_DROP_TEST_HARNESS_H_