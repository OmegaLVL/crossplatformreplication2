
#include "Server.h"
#include "GameObjectRegistry.h"
#include "StringUtils.h"
#include "Colors.h"
#include "PlayerServer.h"
#include "BallServer.h"
#include "RockServer.h"
#include "RandGen.h"


//uncomment this when you begin working on the server

bool Server::StaticInit()
{
	sInstance.reset( new Server() );

	return true;
}

Server::Server()
{
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'PLYR' , PlayerServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'BALL' , BallServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'ROCK' , RockServer::StaticCreate);

	InitNetworkManager();

	// Setup latency
	float latency = 0.0f;
	string latencyString = StringUtils::GetCommandLineArg( 2 );
	if( !latencyString.empty() )
	{
		latency = stof( latencyString );
	}
	NetworkManagerServer::sInstance->SetSimulatedLatency( latency );
}


int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	string portString = StringUtils::GetCommandLineArg( 1 );
	uint16_t port = stoi( portString );

	return NetworkManagerServer::StaticInit( port );
}

//namespace
//{
//
//	void CreateRandomPlayers(int inPlayerCount)
//	{
//		Vector3 playerMin(-5.f, -3.f, 0.f);
//		Vector3 playerMax(5.f, 3.f, 0.f);
//		GameObjectPtr go;
//
//		for (int i = 0; i < inPlayerCount; ++i)
//		{
//			go = GameObjectRegistry::sInstance->CreateGameObject('PLYR');
//			Vector3 playerLocation = RandGen::GetRandomVector(playerMin, playerMax);
//			go->SetLocation(playerLocation);
//		}
//	}
//
//}


void Server::SetupWorld()
{
	//CreateRandomPlayers(3);
	SpawnRock();
}

void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnPlayers();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();
}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{

	int playerId = inClientProxy->GetPlayerId();

	//ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName() );
	SpawnPlayer( playerId );
}

void Server::SpawnPlayer( int inPlayerId )
{
	PlayerPtr player = std::static_pointer_cast< Player >( GameObjectRegistry::sInstance->CreateGameObject( 'PLYR' ) );
	//player->SetColor( Colors::Red );//ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	player->SetPlayerId( inPlayerId );
	//gotta pick a better spawn location than this...
	player->SetLocation( Vector3( 1.f - static_cast< float >( inPlayerId ), 0.f, 0.f ) );

}

void Server::SpawnBall(int inPlayerId)
{
	BallPtr ball = std::static_pointer_cast<Ball>(GameObjectRegistry::sInstance->CreateGameObject('BALL'));
	//ball->SetColor(Colors::Blue);
	ball->SetLocation(Vector3(1.f - static_cast<float>(inPlayerId), 10.0f, 3.0f));
}

void Server::SpawnRock()
{
	RockPtr rock = std::static_pointer_cast<Rock>(GameObjectRegistry::sInstance->CreateGameObject('ROCK'));
	rock->SetColor(Colors::White);
	rock->SetLocation(Vector3(0.0f,0.0f,0.0f));
//	/*RockPtr rock2 = std::static_pointer_cast<Rock>(GameObjectRegistry::sInstance->CreateGameObject('ROCK'));
//	rock->SetColor(Colors::White);
//	rock->SetLocation(Vector3(3.0f, 1.0f, 4.0f));
//	RockPtr rock3 = std::static_pointer_cast<Rock>(GameObjectRegistry::sInstance->CreateGameObject('ROCK'));
//	rock->SetColor(Colors::White);
//	rock->SetLocation(Vector3(11.0f, 1.2f, 6.0f));*/
}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's player
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	//ScoreBoardManager::sInstance->RemoveEntry( playerId );
	PlayerPtr player = GetPlayer( playerId );
	if( player )
	{
		player->SetDoesWantToDie( true );
	}
}

PlayerPtr Server::GetPlayer( int inPlayerId )
{
	//run through the objects till we find the Player...
	//it would be nice if we kept a pointer to the Player on the clientproxy
	//but then we'd have to clean it up when the Player died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];

		/* Original code did this in a weird way, used a method in the base (GameObject) which
		returned a player object if a game object is the player and null otherwise */

		uint32_t type = go->GetClassId();

		//Player* player = dynamic_cast<Player*>(*go);
		PlayerPtr player = nullptr;
		if(type == 'PLYR')
		{
			player = std::static_pointer_cast< Player >(go);
		}

		if(player && player->GetPlayerId() == inPlayerId )
		{
			return player;
		}
	}

	return nullptr;

}
