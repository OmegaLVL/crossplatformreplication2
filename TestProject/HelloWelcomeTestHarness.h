#ifndef HELLO_WELCOME_TEST_HARNESS_H_
#define HELLO_WELCOME_TEST_HARNESS_H_
#pragma once

#include <limits.h>
#include <gtest/gtest.h>

class HelloWelcomeTestHarness : public ::testing::Test
{
protected:

	virtual void SetUp();
	virtual void TearDown();

	// No test object here - its a Singleton.

	GameObjectPtr go;

public:
	HelloWelcomeTestHarness();
	virtual ~HelloWelcomeTestHarness();
};

#endif // HELLO_WELCOME_TEST_HARNESS_H_