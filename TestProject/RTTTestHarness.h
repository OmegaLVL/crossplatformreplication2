#ifndef RTT_TEST_HARNESS_H_
#define RTT_TEST_HARNESS_H_
#pragma once

#include <limits.h>
#include <gtest/gtest.h>

class RTTTestHarness : public ::testing::Test
{
protected:

	virtual void SetUp();
	virtual void TearDown();

	// No test object here - its a Singleton.

	GameObjectPtr go;

public:
	RTTTestHarness();
	virtual ~RTTTestHarness();
};

#endif // RTT_TEST_HARNESS_H_
