#include "RockClient.h"
#include "Rock.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

#include "StringUtils.h"

RockClient::RockClient()
{
	//Reset sprite
	mSpriteComponent.reset( new SpriteComponent( this ) );
	//Set texture to 'rock'
	mSpriteComponent->SetTexture( TextureManager::sInstance->GetTexture( "rock" ) );
}


void RockClient::Update()
{
	
}

void RockClient::Read(InputMemoryBitStream& inInputStream)
{
	bool stateBit;

	uint32_t readState = 0;

	Vector3 oldLocation = GetLocation();
	
	Vector3 replicatedLocation;

	//inInputStream.Read(stateBit);
	//if (stateBit)
	//{
	//	uint32_t playerId;
	//	inInputStream.Read(playerId);
	//	SetPlayerId(playerId);
	//	readState |= ECRS_PlayerId;
	//}

	inInputStream.Read(stateBit);
	if (stateBit)
	{

		inInputStream.Read(replicatedLocation.mX);
		inInputStream.Read(replicatedLocation.mY);

		SetLocation(replicatedLocation);

		readState |= EYRS_Pose;
	}

	inInputStream.Read(stateBit);
	if (stateBit)
	{
		Vector3 color;
		inInputStream.Read(color);
		SetColor(color);
		readState |= EYRS_Color;
	}



}