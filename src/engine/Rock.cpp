#include "Rock.h"
#include "Maths.h"
#include "InputState.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "Player.h"



Rock::Rock() : GameObject(), mPlayerId(0)
{
	SetScale(GetScale() * 0.25f);
	SetCollisionRadius(0.125f);
}

void Rock::Update()
{}


//Vector3 Rock::rLocation(float rX, float rY)
//{
//	rX = 3.0f;
//	rY = 1.0f;
//}

uint32_t Rock::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	//if (inDirtyState & ECRS_PlayerId)
	//{
	//	inOutputStream.Write((bool)true);
	//	inOutputStream.Write(GetPlayerId());

	//	writtenState |= ECRS_PlayerId;
	//}

	if (inDirtyState & EYRS_Pose)
	{
		inOutputStream.Write((bool)true);

		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		writtenState |= EYRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & EYRS_Color)
	{
		inOutputStream.Write((bool)true);

		inOutputStream.Write(GetColor());

		writtenState |= EYRS_Color;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}


	return writtenState;
}

bool Rock::HandleCollisionWithPlayer(Player* inPlayer)
{
	(void)inPlayer;

	return false;
}


