#ifndef BALL_SERVER_H
#define BALL_SERVER_H

#include "Ball.h"
#include "NetworkManagerServer.h"


class BallServer : public Ball
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn( new BallServer() ); }
	void HandleDying() override;

	virtual bool HandleCollisionWithPlayer(Player* inPlayer) override;
	
	virtual void Update() override;

protected:
	BallServer();

private:
	float mTimeToDie;

};

#endif // BALL_SERVER_H
