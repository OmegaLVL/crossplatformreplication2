#ifndef ROCK_H_
#define ROCK_H_

#include "GameObject.h"
#include "World.h"


class Rock : public GameObject
{
public:
	CLASS_IDENTIFICATION('ROCK', GameObject)

		//Setting some attributes and bitshifting.
	enum ERockReplicationState
	{
		EYRS_Pose = 1 << 0,
		EYRS_Color = 1 << 1,

		EYRS_AllState = EYRS_Pose | EYRS_Color
	};


	//Create a Rock.
	static	GameObjectPtr StaticCreatePtr() { return GameObjectPtr (new Rock()); }

	virtual uint32_t GetAllStateMask() const override { return EYRS_AllState; }

	/*Vector3 rLocation(float rX, float rY);*/
	
	void		SetPlayerId(uint32_t inPlayerId) { mPlayerId = inPlayerId; }
	uint32_t	GetPlayerId()						const { return mPlayerId; }

	virtual uint32_t Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const override;

	//Update function.
	virtual void Update() override;

	virtual bool HandleCollisionWithPlayer(Player* inPlayer) override;


protected:
	Rock();

	int mPlayerId;
};

typedef shared_ptr< Rock >	RockPtr;

#endif // ROCK_H_
