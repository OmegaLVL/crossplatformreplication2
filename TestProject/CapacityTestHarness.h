#ifndef CAPACITY_TEST_HARNESS_H_
#define CAPACITY_TEST_HARNESS_H_
#pragma once

#include <limits.h>
#include <gtest/gtest.h>

class CapacityTestHarness : public ::testing::Test
{
protected:

	virtual void SetUp();
	virtual void TearDown();

	// No test object here - its a Singleton.

	GameObjectPtr go;

public:
	CapacityTestHarness();
	virtual ~CapacityTestHarness();
};

#endif // CAPACITY_TEST_HARNESS_H_