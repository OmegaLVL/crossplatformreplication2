#ifndef BALL_CLIENT_H
#define BALL_CLIENT_H

#include "Ball.h"
#include "SpriteComponent.h"

class BallClient : public Ball
{
public:
	static	GameObjectPtr	StaticCreate()		{ return GameObjectPtr( new BallClient() ); }
	
	virtual void Read( InputMemoryBitStream& inInputStream ) override;

protected:
	BallClient();

private:
	SpriteComponentPtr	mSpriteComponent;
};
typedef shared_ptr< BallClient >	BallClientPtr;
#endif //BALL_CLIENT_H
